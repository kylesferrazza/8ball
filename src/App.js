import React from 'react';
import './App.css';
import getResponse from './responses';

function App() {
  const response = getResponse();
  return (
    <div className="App">
      <header className="App-header">
        <p>
          {response}
        </p>
      </header>
    </div>
  );
}

export default App;
